import os
import re
import jinja2
import webapp2

# Variable to store location of templates for jinja2
template_dir = os.path.join(os.path.dirname(__file__), 'templates')

# Jinja environment using template_dir
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape=True)

# Class helper function
class Handler(webapp2.RequestHandler):
    """
    Function:
        This class acts as a helper function to shorten syntax and simplify passing arguments to webapp2 functionality.

        For example, render allows you to pass parameters to a html template file (instead of using webapp2's response.out.write) by self.render(template, **params)

        The functions write and render_str are never called directly, but used in the functionality of render.
    """
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

class MainPage(Handler):
    """
    Function:
        Handles the main page, which is the home of our signup form.

        The main functionality is to validate the user input on the post method, or submit, of the form. This is done by storing the user input in a unique variable and then running that variable through a validation function. This allows us to refer to eacher the validation response or the user input freely.
    """
    def get(self):
        self.render('signup.html')

    def post(self):
        # Store the values entered by the user in the form
        user_username = self.request.get('username')
        user_password = self.request.get('password')
        user_verify = self.request.get('verify')
        user_email = self.request.get('email')

        # Store the regexp.match() object of the user input
        username = valid_username(user_username)
        password = valid_password(user_password)
        verify = valid_verify(user_password, user_verify)
        email = valid_email(user_email)

        # Can only redirect if all validation passes
        # If not all the following variables are matched, re-render form
        if not (username   and   password   and   verify):
            # Normally, it would be a waste of memory to store these variables
            # Outside of their respective if statements, but they need to be
            # Referenced regardless of whether they have any contents until
            # the program gets smarter when re-rendering
            invalid_username = ''
            invalid_password = ''
            invalid_verify = ''
            invalid_email = ''
            if not username:
                invalid_username = "username, "
            if not password:
                invalid_password = "password, "
            if not verify:
                invalid_verify = 'password confirmation, '
            if not email:
                invalid_email = 'email'


            self.render('signup.html',
            error=
                "Input not valid: " +
                invalid_username +
                invalid_password +
                invalid_verify +
                invalid_email +
            ".",
            username=user_username,
            password=user_password,
            verify=user_verify,
            email=user_email)
        else:
            # Method to redirect w/ Google App Engine
            # Redirects to the welcome page using the WelcomePage class
            self.redirect('/welcome?username='+user_username)
            # self.render('welcome.html', username=user_username)


# Handles redirecting if the information is all valid
class WelcomePage(Handler):
    """
    Function:
        When a user inputs all valid inputs, MainPage redirects through webapp2's WSGIApplication, which calls the get method of WelcomePage.

        To pass the username from the redirect, you must get the username and pass it through to the render of the get function.
    """
    def get(self):
        username = self.request.get('username')
        if valid_username(username):
            self.render('welcome.html', username = username)
        else:
            self.redirect('/')

def valid_username(username):
    """
    Function:
        Validating using this regular expression:

        ^[a-zA-Z0-9_-]{3,20}$

        valid_exp stores the regular expression for usernames.
        valid checks for a match between the compiled expression and a passed in string.

    """
    valid_exp = "^[a-zA-Z0-9_-]{3,20}$"
    valid = re.match(valid_exp, username)
    return valid

def valid_password(password):
    """
    Function:
        Validating using this regular expression:

        ^.{3,20}$

        valid_exp stores the regular expression for usernames.
        valid checks for a match between the compiled expression and a passed in string.

    """
    valid_exp = "^.{3,20}$"
    valid = re.match(valid_exp, password)
    return valid

def valid_verify(password, verify):
    """
    Function:
        return whether verify matches password
    """
    if verify == password:
        return True
    else:
        return False

def valid_email(email):
    """
    Function:
        Validating using this regular expression:

        ^[\S]+@[\S]+.[\S]+$

        valid_exp stores the regular expression for usernames.
        valid checks for a match between the compiled expression and a passed in string.
    """
    valid_exp = "^[\S]+@[\S]+.[\S]+$"
    valid = re.match(valid_exp, email)
    return valid

app = webapp2.WSGIApplication([('/', MainPage), ('/welcome', WelcomePage)], debug=True)
