# Back End User Authentication
Signup page that validates user input:
  * name
  * password
  * password confirmation
  * email (optional)

## Languages, frameworks, libraries, etc.
1. Python
  * jinja2
  * webapp2
  * os
  * re
2. HTML
3. CSS
4. Google App Engine

### Validation Logic (Pseudo-code)
```
if inputs valid:
  redirect to welcome page
  "Welcome, {% username %}"
else:
  re-render form
  display error message
  retain username, email inputs
```

### Validation Functions Logic
```
def valid_username(username):
  valid_exp = 'r="^[a-zA-Z0-9_-]{3,20}$"'
  valid = re.match(valid_exp, username)
  return valid
```

### Potential Refractoring
I'm pretty certain there is a way to rewrite the validation functions for username, password (not verify), and email into a single function. The only issue is knowing which regular expression to use when you run that single function.
